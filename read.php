<?php
    require 'database.php';
    $d = new \practice\customer\Database();

    $data = array();
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: index.php");
    } 
    else 
    {
        $data = $d->select("SELECT * FROM customers where id = $id");
    }
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Customer Record</title>
    <meta charset="utf-8">
    <link   href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
        <div class="span10 offset1">
            <div class="row">
                <h3>View Customer Record</h3>
            </div>
                 
                    <div class="form-horizontal">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <p class="form-control-static"><?php echo $data[0]['name']; ?></p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Email Address</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <?php echo $data[0]['email'];?>
                            </p>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Mobile Number</label>
                        <div class="col-sm-10">
                            <p class="form-control-static">
                                <?php echo $data[0]['mobile'];?>
                            </p>
                        </div>
                      </div>
                        <div class="form-group">
                          <a class="btn btn-default" href="index.php">Back</a>
                       </div>
                    </div>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>