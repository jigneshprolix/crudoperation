<?php
  $sort="ASC";
  if(isset($_GET["sort"]))
  {
    if($_GET['sort']=='ASC')
    {
      $sort='DESC';
    }
    else
    {
      $sort='ASC';
    }
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>View Customer Records</title>
    <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
            <div class="row">
                <h3>CRUD Operations</h3>
            </div>
            <div class="row">
                <p>
                    <a href="create.php" class="btn btn-success">Create</a>
                </p>
                <table class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>
                        <a href="index.php?sort=<?php echo $sort; ?>">
                          Name
                          <?php
                            if($sort=='ASC')
                              echo "<span class='glyphicon glyphicon-triangle-bottom'></span>";
                            else
                              echo "<span class='glyphicon glyphicon-triangle-top'></span>";
                          ?>
                        </a>
                      </th>
                      <th>Email Address</th>
                      <th>Mobile Number</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  <?php
                    include 'database.php';
                    $d = new \practice\customer\Database();

                    if (isset($_GET["page"])) 
                    { 
                      $page  = $_GET["page"]; 
                    } 
                    else 
                    { 
                      $page=1; 
                    }

                    $pagesize = 5;
                    $start_from = ($page-1) * $pagesize;
                    $d->display_data_as_table($sort, $start_from, $pagesize);
                  ?>
                  <tr>
                    <td align="center" colspan="4">

                      <?php

                        $total_records = $d->get_total_records("SELECT COUNT(id) FROM customers");
                        $total_pages = ceil($total_records / $pagesize);
                        
                        if($pagesize<$total_records)
                        {

                          if($sort=='ASC')
                            $sort='DESC';
                          else
                            $sort='ASC';

                          echo "<ul class='pagination'>";
                          for ($i=1; $i<=$total_pages; $i++) 
                          {
                            $css = "";
                            if($page==$i)
                              $css = "class='active'";
                        
                            echo "<li " . $css . " ><a href='index.php?sort=$sort&page=".$i."' class='pagination-css'>" . $i . "</a></li>";
                          }
                          echo "</ul>";
                        }
                    ?>
                    </td>
                  </tr>
                  </tbody>
            </table>
        </div>
    </div> <!-- /container -->
  </body>
</html>