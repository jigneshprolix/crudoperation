<?php
    require 'database.php';
    $d = new \practice\customer\Database();

    $id = null;
    if ( !empty($_GET['id'])) 
    {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) 
    {
        header("Location: index.php");
    }
     
    if ( !empty($_POST)) 
    {
        $nameError = null;
        $emailError = null;
        $mobileError = null;
         
        $mob="/^[789][0-9]{9}$/";

        $name = $_POST['name'];
        $email = $_POST['email'];
        $mobile = $_POST['mobile'];
         
        $valid = true;
        if (empty($name)) 
        {
            $nameError = 'Please enter Name';
            $valid = false;
        }
         
        if (empty($email)) 
        {
            $emailError = 'Please enter Email Address';
            $valid = false;
        } 
        else if ( !filter_var($email,FILTER_VALIDATE_EMAIL) ) 
        {
            $emailError = 'Please enter a valid Email Address';
            $valid = false;
        }
         
        if (empty($mobile)) {
            $mobileError = 'Please enter Mobile Number';
            $valid = false;
        }
        else if(!preg_match($mob, $mobile))
        {
            $mobileError = 'Invalid Mobile Number';
            $valid = false;
        }

        if ($valid) 
        {
            $d->query("UPDATE customers  set name = '$name', email = '$email', mobile = '$mobile' WHERE id = $id");
            header("Location: index.php");
        }
    } 
    else 
    {
        $data = $d->select("SELECT * FROM customers where id = $id");
        $name = $data[0]['name'];
        $email = $data[0]['email'];
        $mobile = $data[0]['mobile'];
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Update Customer Record</title>
    <meta charset="utf-8">
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="bootstrap/js/bootstrap.min.js"></script>
</head>
 
<body>
    <div class="container">
     
                <div class="span10 offset1">
                    <div class="row">
                        <h3>Update a Customer</h3>
                    </div>
             
                    <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post">
                      <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
                        <label class="control-label">Name</label>
                        <div class="controls">
                            <input name="name" type="text"  placeholder="Name" value="<?php echo !empty($name) ? $name:'';?>">
                            <?php if (!empty($nameError)): ?>
                                <p class="text-danger"><?php echo $nameError;?></p>
                            <?php endif; ?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
                        <label class="control-label">Email Address</label>
                        <div class="controls">
                            <input name="email" type="text" placeholder="Email Address" value="<?php echo !empty($email)?$email:'';?>">
                            <?php if (!empty($emailError)): ?>
                                <p class="text-danger"><?php echo $emailError;?></p>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="control-group <?php echo !empty($mobileError)?'error':'';?>">
                        <label class="control-label">Mobile Number</label>
                        <div class="controls">
                            <input name="mobile" type="text"  placeholder="Mobile Number" value="<?php echo !empty($mobile)?$mobile:'';?>">
                            <?php if (!empty($mobileError)): ?>
                                <p class="text-danger"><?php echo $mobileError;?></p>
                            <?php endif;?>
                        </div>
                      </div>
                      <div class="form-actions">
                          <button type="submit" class="btn btn-success">Update</button>
                          <a class="btn btn-default" href="index.php">Back</a>
                        </div>
                    </form>
                </div>
                 
    </div> <!-- /container -->
  </body>
</html>