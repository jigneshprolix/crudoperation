<?php
namespace practice\customer;

class Database
{
    private static $dbName = 'customer_db' ;
    private static $dbHost = 'localhost' ;
    private static $dbUsername = 'root';
    private static $dbUserPassword = 'root';
     
    private static $cont = null;
     
    public function __construct() 
    {

    }
     
    public static function connect()
    {
       if ( null == self::$cont )
       {     
        try
        {
          self::$cont =  new \PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword); 
        }
        catch(PDOException $e)
        {
          die($e->getMessage()); 
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
    public function query($query) 
    {
        $connection = $this->connect();
        $result = $connection->query($query);
        return $result;
    }
    public function select($query) 
    {
        $rows = array();
        $result = $this->query($query);
        if($result === false) 
        {
            return false;
        }
        while ($row = $result->fetch(\PDO::FETCH_ASSOC)) 
        {
            $rows[] = $row;
        }
        return $rows;
    }
    public function display_data_as_table($sort, $start_from, $pagesize)
    {
      $sql = "SELECT * FROM customers ORDER BY name $sort LIMIT $start_from, $pagesize";
      foreach ($this->query($sql) as $row) {
          echo '<tr>';
          echo '<td>'. $row['name'] . '</td>';
          echo '<td>'. $row['email'] . '</td>';
          echo '<td>'. $row['mobile'] . '</td>';
          echo '<td width=250>';
            echo '<a class="btn btn-default" href="read.php?id='.$row['id'].'">Read</a>';
            echo ' ';
            echo '<a class="btn btn-success" href="update.php?id='.$row['id'].'">Update</a>';
            echo ' ';
            echo '<a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Delete</a>';
          echo '</td>';
          echo '</tr>';
      }
    }
    public function get_total_records($query)
    {
      $result = $this->query($query);
      $row = $result->fetch(\PDO::FETCH_NUM);
      return $row[0];
    }
}
?>